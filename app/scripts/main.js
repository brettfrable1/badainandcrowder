(function(){

    'use strict';

    const scrollTop = function() {
        $('html, body').animate({ scrollTop: '0px' });
    };

    const init = function() {

        $('.js-scroll-top').on('click', () => {
            scrollTop();
        });

        $('.js-toggle').each(function() {
            $(this).on('click', function() {
                $('body').toggleClass('mobile-nav-open');
            });
        });
    };

    init();

}());
